﻿using FlagGame.Models;
using FlagGame.Repositories;
using System.Net;
using System.Web.Mvc;

namespace FlagGame.Controllers
{
    public class GamesController : Controller
    {
        private IGameRepository repository;

        public GamesController(IGameRepository repository)
        {
            this.repository = repository;
        }
        // GET: Games
        public ActionResult Index()
        {
            return View(repository.All());
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserName")] Game game)
        {
            if (ModelState.IsValid)
            {
                repository.Save(game);                
                return RedirectToAction("Details", new { id = game.ID });
            }

            return View(game);
        }

        // GET: Games/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var game = repository.Find(id.Value);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }
    }


}