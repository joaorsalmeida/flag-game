﻿using FlagGame.Repositories;
using System.Net;
using System.Web.Mvc;

namespace FlagGame.Controllers
{
    public class QuestionsController : Controller
    {
        private IGameRepository repository;

        public QuestionsController(IGameRepository repository)
        {
            this.repository = repository;
        }

        // GET: Questions/Details/5?gameId=1
        public ActionResult Details(int? id, int? gameId)
        {
            if (id == null && gameId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var game = repository.Find(gameId.Value);
            if (game == null) { return HttpNotFound(); }

            var question = game.Questions.Find(x => x.ID == id.Value);
            if (question == null) { return HttpNotFound(); }
            return View(question);
        }
    }
}