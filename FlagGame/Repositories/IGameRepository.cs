﻿using FlagGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlagGame.Repositories
{
    public interface IGameRepository
    {
        List<Game> All();
        Game Save(Game g);
        Game Find(int id);
    }
}
