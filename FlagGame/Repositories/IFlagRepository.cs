﻿using FlagGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlagGame.Repositories
{
    interface IFlagRepository
    {
        List<Flag> All();
        Flag Find(int id);
    }
}
