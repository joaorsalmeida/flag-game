﻿using FlagGame.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlagGame.Repositories
{
    public class GameRepository : IGameRepository
    {
        private static List<Game> games = new List<Game>();

        public List<Game> All()
        {
            return games;
        }

        public Game Save(Game game)
        {
            game.ID = nextID();
            games.Add(game);
            return game;
        }

        public Game Find(int id)
        {
            return games.Where(x => x.ID == id).First();
        }


        private int nextID()
        {
            return games.Count() + 1;
        }
    }
}