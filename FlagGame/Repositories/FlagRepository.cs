﻿using FlagGame.Models;
using System.Collections.Generic;
using System.Linq;

namespace FlagGame.Repositories
{
    public class FlagRepository : IFlagRepository
    {
        private static List<Flag> flags = GenerateFlags();

        public List<Flag> All()
        {
            return flags;
        }

        public Flag Find(int id)
        {
            return flags.First<Flag>(x => x.ID == id);
        }

        private int nextID()
        {
            return All().Count() + 1;
        }

        private static List<Flag> GenerateFlags()
        {
            var flags = new List<Flag>();
            flags.Add(new Flag(flags.Count + 1, "Brazil", "brazil.png"));
            flags.Add(new Flag(flags.Count + 1, "USA", "usa.png"));
            flags.Add(new Flag(flags.Count + 1, "Spain", "spain.png"));
            flags.Add(new Flag(flags.Count + 1, "Chile", "chile.png"));
            flags.Add(new Flag(flags.Count + 1, "UK", "uk.png"));
            return flags;
        }
    }
}