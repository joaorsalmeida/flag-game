﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlagGame.Models
{
    public class Game
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public List<Question> Questions { get; }

        public Game()
        {
            var questionsGenerator = new QuestionsGenerator();
            Questions = questionsGenerator.Generate();
        }

        public Question NextQuestion()
        {
           return Questions.Where(x => x.Answered() == false).FirstOrDefault();
        }
    }
}