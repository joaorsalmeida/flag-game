﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlagGame.Models
{
    public class Question
    {

        public int ID { get; }
        public int Score { get; set; }
        public Flag Flag { get; set; }
        public List<String> WrongOptions { get; set; }
        public string answer { get; set; }

        public Question(int id, int score, Flag flag, List<string> wrongOptions)
        {
            ID = id;
            Score = score;
            Flag = flag;
            WrongOptions = wrongOptions;
        }
        
        public List<String> Options()
        {
            var random = new Random();
            var options = WrongOptions;
            options.Add(Flag.CountryName);
            return options.OrderBy(order => random.Next()).ToList();
        }

        public bool Answered()
        {
            return answer != null;
        }

    }
}