﻿using FlagGame.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FlagGame.Models
{
    public class QuestionsGenerator
    {

        private List<Flag> flags;

        public QuestionsGenerator()
        {
            flags = new FlagRepository().All();
        }

        public List<Question> Generate()
        {
            var questions = new List<Question>();
            
            foreach(var flag in flags)
            {
                var score = new Random().Next(100);
                var wrongOptions = GenerateWrongOptions(flag);
                questions.Add(new Question(questions.Count + 1, score, flag, wrongOptions));
            }
            return questions;
        }

        private List<String> GenerateWrongOptions(Flag flag)
        {
            return flags.Where(x => x.ID != flag.ID).Take(3).Select(f => f.CountryName).ToList();
        }
    }
}