﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlagGame.Models
{
    public class Flag
    {
        public int ID { get; }
        public string CountryName { get; set; }
        public string FlagFile { get; set; }

        public Flag(int id, string countryName, string flagFile)
        {
            ID = id;
            CountryName = countryName;
            FlagFile = flagFile;
        }
    }
}